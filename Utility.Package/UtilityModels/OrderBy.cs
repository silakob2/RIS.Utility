﻿using System.ComponentModel.DataAnnotations;

namespace Utility.Package.UtilityModels;

public class OrderBy
{
    [Required]
    public string Field { get; set; }
    [Required]
    public bool ByDescending { get; set; } = false;
}