namespace Utility.Package.UtilityModels;

[Serializable]
public class ApiException : Exception
{
    public string ErrorCode { get; set; }
    public string MessageDesc { get; set; }
    public string Url { get; set; }
    public string? RequestBody { get; set; }
    public object? Result { get; set; }

    public ApiException() : base()
    {
    }

    public ApiException(string errorCode, string messageDesc, string url, string? requestBody = null, object? result = null) : base(messageDesc)
    {
        ErrorCode = errorCode;
        MessageDesc = messageDesc;
        Url = url;
        RequestBody = requestBody;
        Result = result;
    }
}