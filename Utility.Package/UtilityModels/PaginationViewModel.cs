﻿namespace Utility.Package.UtilityModels;

public class PaginationViewModel
{
    public PaginationViewModel()
    {
    }

    public PaginationViewModel(int totalItem, int totalPage, int currentPage)
    {
        TotalItem = totalItem;
        TotalPage = totalPage;
        CurrentPage = currentPage;
    }

    public int TotalItem { get; set; }
    public int TotalPage { get; set; }
    public int CurrentPage { get; set; }
}