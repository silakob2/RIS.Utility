﻿namespace Utility.Package.UtilityModels;

public class Actor
{
    public string? ActorId { get; set; }
    public string? ActorBy { get; set; }
    public string? ClaimCompanyIds { get; set; }
    public string? ClaimCompanyCodes { get; set; }
    public string? ClaimCompanyNames { get; set; }
    public string? ClaimApplicationIds { get; set; }
    public string? ClaimApplicationCodes { get; set; }
    public string? ClaimApplicationNames { get; set; }
    public string? ClaimRoleIds { get; set; }
    public string? ClaimRoleCodes { get; set; }
    public string? ClaimRoleNames { get; set; }
    public string? ClaimVendorCode { get; set; }
    public string? ClaimCompanyBranch { get; set; }
    public string SessionId { get; set; }

    public bool IsValidUserId(string? userId)
    {
        if (ClaimRoleIds!.Split(",").Contains("4"))
        {
            return true;
        }
        if (!ClaimRoleIds!.Split(",").Contains("4") && userId == ActorId)
        {
            return true;
        }
        return false;
    }
}