using System.ComponentModel.DataAnnotations;

namespace Utility.Package.UtilityModels;

public class SearchQuery
{
    public bool? IsActive { get; set; }
    [Required]
    public string Field { get; set; }
    public bool ByDescending { get; set; }
    public int CurrentPage { get; set; } = 1;
    public int ItemPerPage { get; set; } = 10;
}