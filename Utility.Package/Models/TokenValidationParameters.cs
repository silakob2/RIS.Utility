﻿using Microsoft.IdentityModel.Tokens;

namespace Utility.Package.Models;

public class TokenValidationParameters
{
    public bool RequireExpirationTime { get; set; }
    public bool ValidateLifetime { get; set; }
    public bool ValidateIssuer { get; set; }
    public bool ValidateAudience { get; set; }
    public bool RequireSignedTokens { get; set; }
    public RsaSecurityKey IssuerSigningKey { get; set; }
}