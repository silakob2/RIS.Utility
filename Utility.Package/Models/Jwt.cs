﻿namespace Utility.Package.Models;

public class Jwt
{
    public List<string> AnonymousEndPoints { get; set; }
    public string PublicKey { get; set; }
    public string PrivateKey { get; set; }
    public int ExpireIn { get; set; }
    public int RefreshTokenExpireIn { get; set; }
    public string GetSessionBySessionIdUrl { get; set; }
    public List<string>? RestrictedByRoleIdEndPoints { get; set; }
    public List<string> AllowRolesAccess { get; set; } = new();
}