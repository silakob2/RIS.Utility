﻿using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Utility.Package.Models;
using Utility.Package.UtilityModels;
using Utility.Package.UtilityServices;
using Utility.Package.UtilityServices.Interfaces;

namespace Utility.Package.Middlewares;

public class SprintConnectMiddleWareService
{
    private readonly IEncryptionService _encryptionService;
    private readonly IClaimManagementService _claimManagementService;
    private readonly Jwt _jwt;
    private readonly ILogger<SprintConnectMiddleWareService> _logger;
    private readonly RequestDelegate _next;

    public SprintConnectMiddleWareService(RequestDelegate next, IOptions<Jwt> jwt,
        IEncryptionService encryptionService,
        IClaimManagementService claimManagementService,
        ILoggerFactory loggerFactory)
    {
        _next = next;
        _jwt = jwt.Value;
        _encryptionService = encryptionService;
        _claimManagementService = claimManagementService;
        _logger = loggerFactory.CreateLogger<SprintConnectMiddleWareService>();
    }

    public async Task InvokeAsync(HttpContext context, Actor actor)
    {
        if (_jwt.AnonymousEndPoints != null &&
            _jwt.AnonymousEndPoints.Exists(
                endpoint => context.Request.Path.Value.ToLower().Contains(endpoint.ToLower())))
        {
            //Process API
            await _next(context);
            return;
        }

        // Validate Authenication
        if (!context.Request.Headers.TryGetValue("Authorization", out var token))
        {
            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            context.Response.Headers.Add("unauthorize-error", "Authorization Not Found");
            await context.Response.WriteAsync("Authorization Not Found");
            return;
        }


        var tokenValidationParameters = JwtService.GetTokenValidationParameters(_jwt.PublicKey);
        var stringToken = token.ToString().Replace("Bearer ", "");

        // validate the token
        var handler = new JwtSecurityTokenHandler();
        try
        {
            ClaimsPrincipal claimsPrincipal = handler.ValidateToken(stringToken, tokenValidationParameters, out _);

            // by pass Get GetSessionBySessionId
            var endpointGetSessionBySessionId = context.Request.Path.Value;
            if (!_jwt.GetSessionBySessionIdUrl.Contains(endpointGetSessionBySessionId))
            {
                //set actor
                var claims = _claimManagementService.GetClaims(claimsPrincipal);
                var actorInfo = _claimManagementService.SetSessionIdToActor(claims, _jwt.GetSessionBySessionIdUrl);
                if (actorInfo == null)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    context.Response.Headers.Add("unauthorize-error", "Invalid Validation");
                    await context.Response.WriteAsync("Invalid Validation");
                    return;
                }
                //validate role
                if (!_claimManagementService.ValidateRoleInActor(actorInfo))
                {
                    _logger.LogDebug("validate role : Invalid RoleInActor");
                    context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                    context.Response.Headers.Add("unauthorize-error", "Invalid Role Validation");
                    await context.Response.WriteAsync("Invalid Role Validation");
                    return;
                }
                actor.SessionId = actorInfo.SessionId;
                actor.ActorId = actorInfo.ActorId;
                actor.ActorBy = actorInfo.ActorBy;
                actor.ClaimApplicationIds = actorInfo.ClaimApplicationIds;
                actor.ClaimApplicationCodes = actorInfo.ClaimApplicationCodes;
                actor.ClaimApplicationNames = actorInfo.ClaimApplicationNames;
                actor.ClaimRoleIds = actorInfo.ClaimRoleIds;
                actor.ClaimRoleCodes = actorInfo.ClaimRoleCodes;
                actor.ClaimRoleNames = actorInfo.ClaimRoleNames;
                actor.ClaimCompanyIds = actorInfo.ClaimCompanyIds;
                actor.ClaimCompanyCodes = actorInfo.ClaimCompanyCodes;
                actor.ClaimCompanyNames = actorInfo.ClaimCompanyNames;
                actor.ClaimVendorCode = actorInfo.ClaimVendorCode;
                actor.ClaimCompanyBranch = actorInfo.ClaimCompanyBranch;
            }

            if (!(context.Request.ContentType ?? string.Empty).Contains("multipart/form-data"))
                AddActorBy(context);

            Console.WriteLine("Token validated successfully.");
            _logger.LogDebug("Token validated successfully.");

            await _next(context);
        }
        catch (SecurityTokenException ex)
        {
            Console.WriteLine("Error validating token: " + ex.Message);
            _logger.LogError("Error validating token: " + ex.Message);

            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            context.Response.Headers.Add("unauthorize-error", "Invalid Token");
            await context.Response.WriteAsync("Invalid Token");
        }
        catch (Exception ex)
        {
            if (ex is ValidationException)
            {
                throw;
            }

            context.Response.StatusCode = 400;
            await context.Response.WriteAsync(ex.Message);
        }
    }

    private void AddActorBy(HttpContext context)
    {
        var request = context.Request;
        try
        {
            if (request.Method == "GET" || request.Method == "DELETE")
                ProcessGetDelete(context);
            else
                ProcessPostPutPatch(context);
        }
        catch (Exception ex)
        {
            //log error
            Console.WriteLine($"Token validated successfully, but not completed to add ActorBy: {ex.Message}");
            _logger.LogError($"Token validated successfully, but not completed to add ActorBy: {ex.Message}");
            throw;
        }
    }

    private void ProcessGetDelete(HttpContext context)
    {
        if (context.Request.QueryString.HasValue)
        {
            var queryString = context.Request.Query["param"];
            var decryptedString = _encryptionService.Decrypt(queryString.ToString());
            Console.WriteLine(string.Concat("decryptedString:", decryptedString));
            _logger.LogDebug(string.Concat("decryptedString:", decryptedString));
            dynamic dataSource = JsonConvert.DeserializeObject<dynamic>(decryptedString);
            dataSource.param = queryString.ToString();
            // Convert the JObject to a dictionary      
            var dictionary = dataSource.ToObject<Dictionary<string, object>>();

            // Add the dictionary's key-value pairs to the QueryString collection
            var queryBuilder = new QueryBuilder();
            foreach (var kvp in dictionary)
                if (kvp.Value != null)
                    queryBuilder.Add(kvp.Key, kvp.Value.ToString());

            context.Request.QueryString = queryBuilder.ToQueryString();
        }
    }

    private void ProcessPostPutPatch(HttpContext context)
    {
        //get the request body and put it back for the downstream items to read
        var stream = context.Request.Body; // currently holds the original stream                    
        var originalContent = new StreamReader(stream).ReadToEndAsync();
        var notModified = true;
        var dataSource = JsonConvert.DeserializeObject<dynamic>(originalContent.Result);
        if (dataSource != null)
        {
            //modified stream
            var json = JsonConvert.SerializeObject(dataSource);
            var requestData = Encoding.UTF8.GetBytes(json);
            stream = new MemoryStream(requestData);
            notModified = false;
        }

        if (notModified)
        {
            //put original data back for the downstream to read
            var requestData = Encoding.UTF8.GetBytes(originalContent.Result);
            stream = new MemoryStream(requestData);
        }

        context.Request.Body = stream;
    }
}