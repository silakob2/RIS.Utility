﻿using System.Dynamic;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Utility.Package.Extensions;
using Utility.Package.Models;
using Utility.Package.UtilityServices.Interfaces;

namespace Utility.Package.Middlewares;

public class BodySignatureMiddleware : IMiddleware
{
    private readonly ISignatureService _signatureService;
    private readonly ILogger<BodySignatureMiddleware> _logger;
    private readonly Jwt _jwt;
    private string _bodyString = string.Empty;

    public BodySignatureMiddleware(ISignatureService signatureService,
        IOptions<Jwt> jwt, ILogger<BodySignatureMiddleware> logger)
    {
        _signatureService = signatureService;
        _logger = logger;
        _jwt = jwt.Value;
    }

    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        if (_jwt.AnonymousEndPoints != null &&
            _jwt.AnonymousEndPoints.Exists(
                endpoint => context.Request.Path.Value.ToLower().Contains(endpoint.ToLower())))
        {
            //Process API
            await next(context);
            return;
        }
        
        await SetBodyAsync(context);

        var signature = await ValidHeader(context);
        if (signature.IsNullOrEmptyCore())
        {
            return;
        }

        if (ValidSign(signature))
        {
            await next(context);
        }
        else
        {
            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            context.Response.Headers.Add("unauthorize-error", "Signature Compatible");
            await context.Response.WriteAsync("Signature Compatible");
        }
    }

    #region Private Methods

    private async Task<string> ValidHeader(HttpContext context)
    {
        if (!context.Request.Headers.TryGetValue("kpgHo", out var signature))
        {
            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            context.Response.Headers.Add("unauthorize-error", "ValidHeader Not Found");
            await context.Response.WriteAsync("ValidHeader Not Found");
            return string.Empty;
        }

        if (signature.ToString().IsNullOrEmptyCore())
        {
            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            context.Response.Headers.Add("unauthorize-error", "Signature Not Found");
            await context.Response.WriteAsync("Signature Not Found");
            return string.Empty;
        }

        return signature.ToString();
    }

    private bool ValidSign(string requestSignature)
    {
        string signature = _signatureService.SignSha256(_bodyString);
        Console.WriteLine($"Header: {requestSignature} , Body: {_bodyString} , Signature From Body: {signature}");
        _logger.LogDebug("Header: {RequestSignature} , Body: {BodyString} , Signature From Body: {Signature}", requestSignature, _bodyString, signature);
        return string.Equals(requestSignature, signature);
    }

    private async Task SetBodyAsync(HttpContext context)
    {
        string[] methods = { "GET", "DELETE" };
        if (methods.Contains(context.Request.Method.ToUpper()))
        {
            var param = context.Request.Query["param"].ToString();
            var buildJson = new {param};
            var jsonToBody = JsonConvert.SerializeObject(buildJson);
            _bodyString = jsonToBody;
        }
        else
        {
            if (context.Request.HasFormContentType)
            {
                // Read the form data
                var formCollection = await context.Request.ReadFormAsync();
                // Convert the form data to a dynamic object
                dynamic formData = new ExpandoObject();
                var formDataDictionary = (IDictionary<string, object>)formData;
                foreach (var key in formCollection.Keys)
                {
                    formDataDictionary[key] = formCollection[key][0]!;
                }
                // Convert the dynamic object to JSON
                string jsonData = JsonConvert.SerializeObject(formDataDictionary, new JsonSerializerSettings
                {
                    ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
                });
                _bodyString = jsonData;
            }
            else
            {
                var memoryStream = new MemoryStream();
                context.Request.EnableBuffering();
                await context.Request.Body.CopyToAsync(memoryStream);
                context.Request.Body.Position = 0;
                _bodyString = ReadStreamInChunks(memoryStream);
                await memoryStream.DisposeAsync();
            }
        }
    }

    private string ReadStreamInChunks(Stream stream)
    {
        const int readChunkBufferLength = 4096;
        stream.Seek(0, SeekOrigin.Begin);
        var textWriter = new StringWriter();
        var reader = new StreamReader(stream);
        var readChunk = new char[readChunkBufferLength];
        int readChunkLength;
        do
        {
            readChunkLength = reader.ReadBlock(readChunk, 0, readChunkBufferLength);
            textWriter.Write(readChunk, 0, readChunkLength);
        } while (readChunkLength > 0);

        reader.Dispose();
        return textWriter.ToString();
    }

    #endregion
}