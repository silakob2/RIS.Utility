﻿using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Utility.Package.UtilityServices;

public static class MappingService
{
    public static Destination Map<Source, Destination>(Source model)
    {
        try
        {
            var options = new JsonSerializerOptions
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
                PropertyNameCaseInsensitive = true,
                WriteIndented = true
            };
            var json = JsonSerializer.Serialize(model, options);
            return JsonSerializer.Deserialize<Destination>(json, options);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
            throw;
        }
    }

    public static void MapProp(object sourceObj, object targetObj)
    {
        var T1 = sourceObj.GetType();
        var T2 = targetObj.GetType();

        var sourceProprties = T1.GetProperties(BindingFlags.Instance | BindingFlags.Public);
        var targetProprties = T2.GetProperties(BindingFlags.Instance | BindingFlags.Public);


        for (var i = 0; i < sourceProprties.Length; i++)
        {
            var sourceProp = sourceProprties[i];
            var index = Array.FindIndex(targetProprties, a => a.Name == sourceProp.Name);

            if (index >= 0)
            {
                var osourceVal = sourceProp.GetValue(sourceObj, null);

                var targetProp = targetProprties[index];
                targetProp.SetValue(targetObj, osourceVal);
            }
        }
    }
}