using System.Security.Cryptography;
using System.Text;
using Utility.Package.UtilityServices.Interfaces;

namespace Utility.Package.UtilityServices;

public class EncryptionService : IEncryptionService
{
    private readonly string IV = "initial vector here";
    private readonly string Key = "private key here";

    public byte[] EncryptStringToBytes_Aes(string plainText)
    {
        // Check arguments.
        if (plainText == null || plainText.Length <= 0)
            throw new ArgumentNullException("plainText");
        if (Key == null || Key.Length <= 0)
            throw new ArgumentNullException($"Key: {Key ?? string.Empty}");
        if (IV == null || IV.Length <= 0)
            throw new ArgumentNullException($"IV: {IV ?? string.Empty}");
        byte[] encrypted;

        // Create an Aes object
        // with the specified key and IV.
        var aesAlg = Aes.Create();
        aesAlg.Padding = PaddingMode.PKCS7;
        aesAlg.Mode = CipherMode.ECB;
        aesAlg.KeySize = 128;
        aesAlg.Key = Encoding.UTF8.GetBytes(Key);
        aesAlg.IV = Encoding.UTF8.GetBytes(IV);

        // Create an encryptor to perform the stream transform.
        var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

        // Create the streams used for encryption.
        using (var msEncrypt = new MemoryStream())
        {
            using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
            {
                using (var swEncrypt = new StreamWriter(csEncrypt))
                {
                    //Write all data to the stream.
                    swEncrypt.Write(plainText);
                }

                encrypted = msEncrypt.ToArray();
            }
        }

        aesAlg.Dispose();
        // Return the encrypted bytes from the memory stream.
        return encrypted;
    }
    public string DecryptStringFromBytes_Aes(byte[] cipherText)
    {
        try
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException($"Key: {Key ?? string.Empty}");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException($"IV: {IV ?? string.Empty}");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an Aes object
            // with the specified key and IV.
            var aesAlg = Aes.Create();
            aesAlg.Padding = PaddingMode.PKCS7;
            aesAlg.Mode = CipherMode.ECB;
            aesAlg.KeySize = 128;
            aesAlg.Key = Encoding.UTF8.GetBytes(Key);
            aesAlg.IV = Encoding.UTF8.GetBytes(IV);

            // Create a decryptor to perform the stream transform.
            var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for decryption.
            using (var msDecrypt = new MemoryStream(cipherText))
            {
                using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (var srDecrypt = new StreamReader(csDecrypt))
                    {
                        // Read the decrypted bytes from the decrypting stream
                        // and place them in a string.
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }

            aesAlg.Dispose();
            return plaintext;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    public string Encrypt(string toEncryptString)
    {
        var bytesAes = EncryptStringToBytes_Aes(toEncryptString);
        var encryptedString = Convert.ToBase64String(bytesAes);
        encryptedString = encryptedString.Replace("+", "xMl3Jk").Replace("/", "Por21Ld").Replace("=", "Ml32");
        return encryptedString;
    }
    public string Decrypt(string encryptedString)
    {
        encryptedString = encryptedString.Replace("xMl3Jk", "+").Replace("Por21Ld", "/").Replace("Ml32", "=");
        byte[] encryptedBytes = Convert.FromBase64String(encryptedString);
        return DecryptStringFromBytes_Aes(encryptedBytes);
    }
}