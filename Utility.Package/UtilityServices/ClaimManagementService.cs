﻿using System.Security.Claims;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Utility.Package.Extensions;
using Utility.Package.Models;
using Utility.Package.UtilityModels;
using Utility.Package.UtilityServices.Interfaces;

namespace Utility.Package.UtilityServices;

public class ClaimManagementService : IClaimManagementService
{
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly ISignatureService _signatureService;
    private readonly Jwt _jwt;

    public ClaimManagementService(IHttpContextAccessor httpContextAccessor,
        ISignatureService signatureService,
        IOptions<Jwt> jwt)
    {
        _httpContextAccessor = httpContextAccessor;
        _signatureService = signatureService;
        _jwt = jwt.Value;
    }

    public IEnumerable<Claim> GetClaims(ClaimsPrincipal claimsPrincipal) => claimsPrincipal.Claims;

    public Actor? SetSessionIdToActor(IEnumerable<Claim> claims, string url)
    {
        string encryptedSessionId = claims.FirstOrDefault(x => x.Type == "claimSessionId")?.Value.ToString()!;
        Actor? actor = GetSessionBySessionId(encryptedSessionId, url);
        return actor;
    }

    public bool ValidateRoleInActor(Actor actor)
    {
        var roleIds = actor.ClaimRoleIds!.Split(",");
        if (IsAllowRolesAccess(_jwt.AllowRolesAccess, roleIds))
        {
            return true;
        }

        //validate endpoints
        if (_jwt.RestrictedByRoleIdEndPoints != null)
        {
            if (!_jwt.RestrictedByRoleIdEndPoints.Exists(
                    endpoint => _httpContextAccessor.HttpContext.Request.Path.Value.ToLower()
                        .Contains(endpoint.ToLower())))
            {
                return true;
            }

            return false;
        }

        return true;
    }

    #region Private Methods

    private bool IsAllowRolesAccess(List<string> jwtAllowRolesAccess, string[] roleIds)
    {
        return jwtAllowRolesAccess.Exists(roleIds.Contains);
    }

    private Actor? GetSessionBySessionId(string sessionId, string url)
    {
        try
        {
            GetSessionBySessionIdCommand command = new() { SessionId = sessionId };
            var getSessionBySessionIdJsonString = command.SerializeCamelCaseCore().Replace(" ", "").Replace("\r\n", "");
            var signature = _signatureService.SignSha256(getSessionBySessionIdJsonString);

            // Creating a New Request
            var httpRequest = CreateNewRequest();
            // Creating a KeyValuePair
            KeyValuePair<string, StringValues> pairSignature =
                new KeyValuePair<string, StringValues>("kpgHo", signature);
            httpRequest.Request.Headers.Add(pairSignature);
            KeyValuePair<string, StringValues> pairAuthorization =
                new KeyValuePair<string, StringValues>("Authorization",
                    _httpContextAccessor.HttpContext.Request.Headers["Authorization"]);
            httpRequest.Request.Headers.Add(pairAuthorization);

            SessionResponse session = httpRequest.Request.PostAsyncCore(command, url)
                .ToResponseCore<SessionResponse>(url).Result!;
            Actor actor = new Actor
            {
                SessionId = session.SessionId!,
                ActorId = session.UserId,
                ActorBy = session.Username,
                ClaimCompanyIds = session.CompanyId,
                ClaimCompanyCodes = session.CompanyCode,
                ClaimCompanyNames = session.CompanyName,
                ClaimRoleIds = session.RoleId,
                ClaimRoleCodes = session.RoleCode,
                ClaimRoleNames = session.RoleName,
                ClaimApplicationIds = session.ApplicationId,
                ClaimApplicationCodes = session.ApplicationCode,
                ClaimApplicationNames = session.ApplicationName,
                ClaimVendorCode = session.VendorCode,
                ClaimCompanyBranch = session.CompanyBranch
            };
            return actor;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    private HttpContext CreateNewRequest()
    {
        var httpRequest = new DefaultHttpContext();
        return httpRequest;
    }

    #endregion
}

public class GetSessionBySessionIdCommand
{
    [JsonInclude]
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
    public string? SessionId { get; set; }
}

public class SessionResponse
{
    public string? SessionId { get; set; }
    public string? UserId { get; set; }
    public string? Username { get; set; }
    public string? CompanyId { get; set; }
    public string? CompanyCode { get; set; }
    public string? CompanyName { get; set; }
    public string? RoleId { get; set; }
    public string? RoleCode { get; set; }
    public string? RoleName { get; set; }
    public string? ApplicationId { get; set; }
    public string? ApplicationCode { get; set; }
    public string? ApplicationName { get; set; }
    public string? CompanyBranch { get; set; }
    public string? VendorCode { get; set; }
}