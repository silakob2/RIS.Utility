﻿using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Utility.Package.Extensions;
using Utility.Package.UtilityModels;

namespace Utility.Package.UtilityServices;

public static class QueryService
{
    /// <summary>
    ///     Use For OrderBy, OrderByDescending and Return IOrderedQueryable
    /// </summary>
    /// <typeparam name="T">Your Model</typeparam>
    /// <param name="source">Your IQueryable that you want to order by</param>
    /// <param name="isFirst">Is this the first order by</param>
    /// <returns> IOrderedQueryable</returns>
    public static IOrderedQueryable<T> ApplyOrder<T>(this IQueryable<T> source, OrderBy sort, bool isFirst)
    {
        string methodName;
        if (isFirst)
            methodName = sort.ByDescending ? "OrderByDescending" : "OrderBy";
        else
            methodName = sort.ByDescending ? "ThenByDescending" : "ThenBy";

        var prop = sort.Field;
        var type = typeof(T);
        var arg = Expression.Parameter(type, "x");
        Expression expr = arg;
        var propertyInfo = type.GetProperty(prop);
        expr = Expression.Property(expr, propertyInfo);
        type = propertyInfo.PropertyType;
        var delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
        var lambda = Expression.Lambda(delegateType, expr, arg);

        var result = typeof(Queryable).GetMethods().Single(
                method => method.Name == methodName
                          && method.IsGenericMethodDefinition
                          && method.GetGenericArguments().Length == 2
                          && method.GetParameters().Length == 2)
            .MakeGenericMethod(typeof(T), type)
            .Invoke(null, new object[] { source, lambda });
        return (IOrderedQueryable<T>)result;
    }

    /// <summary>
    ///     Use For OrderBy, OrderByDescending and Return expression function
    /// </summary>
    /// <typeparam name="T">Your Model</typeparam>
    /// <param name="propertyName"> The name of property that you want to order by</param>
    /// <param name="byDescending">Is this order by descending</param>
    /// <returns> Expression Function</returns>
    public static Expression<Func<T, object>> ApplyOrder<T>(this IQueryable<T> source, string propertyName,
        bool byDescending = false)
    {
        var prop = propertyName;
        var type = typeof(T);
        var arg = Expression.Parameter(type, "x");
        Expression expr = arg;
        var pi = type.GetProperty(prop);
        expr = Expression.Property(expr, pi);
        LambdaExpression lambda = Expression.Lambda<Func<T, object>>(expr, arg);
        return (Expression<Func<T, object>>)lambda;
    }

    /// <summary>
    ///     using for apply pagination
    /// </summary>
    /// <typeparam name="T">Model</typeparam>
    /// <param name="source"> Your IQueryable of object to make the pagination </param>
    /// <param name="currentPageNo">Your Current Page</param>
    /// <param name="itemPerPage">Item per Page that you want</param>
    /// <returns></returns>
    public static Tuple<List<T>, PaginationViewModel> ApplyPagination<T>(this IQueryable<T> source, int currentPageNo,
        int itemPerPage)
    {
        if (currentPageNo < 1 || itemPerPage < 1) throw new InvalidDataException("pageNo or itemPerPage is Invalid");
        var totalItem = source.Count();
        var remainItem = 0;
        if (totalItem == 0)
            remainItem = 1;
        else
            remainItem = totalItem % itemPerPage > 0 ? 1 : 0;

        var totalPage = totalItem / itemPerPage + remainItem;
        var result = source.Skip((currentPageNo - 1) * itemPerPage).Take(itemPerPage).ToList();
        return Tuple.Create(result, new PaginationViewModel(totalItem, totalPage, currentPageNo));
    }
    
    public static Tuple<List<T>, PaginationViewModel> ApplyPagination<T>(this IEnumerable<T> source, int currentPageNo,
        int itemPerPage)
    {
        if (currentPageNo < 1 || itemPerPage < 1) throw new InvalidDataException("pageNo or itemPerPage is Invalid");
        var totalItem = source.Count();
        var remainItem = 0;
        if (totalItem == 0)
            remainItem = 1;
        else
            remainItem = totalItem % itemPerPage > 0 ? 1 : 0;

        var totalPage = totalItem / itemPerPage + remainItem;
        var result = source.Skip((currentPageNo - 1) * itemPerPage).Take(itemPerPage).ToList();
        return Tuple.Create(result, new PaginationViewModel(totalItem, totalPage, currentPageNo));
    }

    /// <summary>
    /// </summary>
    /// <param name="_context">your dbcontext</param>
    /// <param name="commonKey">keyword that you want to find in DbSet named by commonKey</param>
    /// <param name="parameters">wording that will concat with description</param>
    /// <typeparam name="T">DbSet to find commonkey</typeparam>
    /// <returns>DbSet</returns>
    public static Tuple<string, string> GetStatusByCommonKey<T>(this DbContext _context, string commonKey,
        params string[] parameters) where T : class
    {
        var type = typeof(T);
        var properties = type.GetProperties();
        var parameterExpression = Expression.Parameter(type, "con");
        var body = Expression.MakeMemberAccess(parameterExpression,
            properties.FirstOrDefault(condition => condition.Name == "CommonKey")!);
        Expression left = body;
        Expression right = Expression.Constant(commonKey);
        Expression searchExpression = Expression.Equal(left, right);
        var unaryExpression =
            Expression.Lambda<Func<T, bool>>(searchExpression, parameterExpression);
        var status = _context.Set<T>().Where(unaryExpression).FirstOrDefault();
        if (status == null) throw new SqlNullValueException("Not Found CommonKey: " + commonKey);

        var modelProperties = status.GetType().GetProperties();
        var statusDisplayLocal = modelProperties.FirstOrDefault(condition => condition.Name == "StatusDisplayLocal");
        var reponseStatusDesc = statusDisplayLocal.GetValue(status, null);
        if (parameters != null && parameters.Length != 0)
            reponseStatusDesc = string.Format(reponseStatusDesc.ToString(), parameters);

        var propertyStatusCode = modelProperties.FirstOrDefault(condition => condition.Name == "StatusCode");
        var reponseStatusCode = propertyStatusCode.GetValue(status, null);
        return new Tuple<string, string>(reponseStatusCode.ToString(), reponseStatusDesc.ToString());
    }
    
    /// <summary>
    /// </summary>
    /// <param name="_context">your dbcontext</param>
    /// <param name="commonKey">keyword that you want to find in DbSet named by commonKey</param>
    /// <param name="parameters">wording that will concat with description</param>
    /// <typeparam name="T">DbSet to find commonkey</typeparam>
    /// <returns>DbSet</returns>
    public static Tuple<int, string> GetStatusByStatusCode<T>(this DbContext _context, string statusCode,
        params string[] parameters) where T : class
    {
        var type = typeof(T);
        var properties = type.GetProperties();
        var parameterExpression = Expression.Parameter(type, "con");
        var body = Expression.MakeMemberAccess(parameterExpression,
            properties.FirstOrDefault(condition => condition.Name == "StatusCode"));
        Expression left = body;
        Expression right = Expression.Constant(statusCode);
        Expression searchExpression = Expression.Equal(left, right);
        var unaryExpression =
            Expression.Lambda<Func<T, bool>>(searchExpression, parameterExpression);
        var status = _context.Set<T>().Where(unaryExpression).FirstOrDefault();
        if (status == null) throw new SqlNullValueException("Not Found StatusCode: " + statusCode);

        var modelProperties = status.GetType().GetProperties();
        var statusDisplayLocal = modelProperties.FirstOrDefault(condition => condition.Name == "StatusDisplayLocal");
        var reponseStatusDesc = statusDisplayLocal.GetValue(status, null);
        if (parameters != null && parameters.Length != 0)
            reponseStatusDesc = string.Format(reponseStatusDesc.ToString(), parameters);

        var propertyStatusCode = modelProperties.FirstOrDefault(condition => condition.Name == "StatusCode");
        var reponseStatusCode = propertyStatusCode.GetValue(status, null);
        return new Tuple<int, string>(reponseStatusCode.ToIntCore() ?? 0, reponseStatusDesc.ToString());
    }

    /// <summary>
    /// </summary>
    /// <param name="id">value of primary key</param>
    /// <typeparam name="TModel"></typeparam>
    /// <returns>optional TModel</returns>
    public static TModel? GetModelByPrimaryKey<TModel>(this DbContext _context, object id) where TModel : class
    {
        TModel? model = null;
        var modelType = typeof(TModel);
        var properties = modelType.GetProperties();
        foreach (var property in properties)
        {
            var isValidKeyAttribute = Attribute.GetCustomAttribute(property, typeof(KeyAttribute)) is KeyAttribute;
            if (isValidKeyAttribute)
            {
                model = _context.Set<TModel>().FirstOrDefault(BuildKeyConditionExpression<TModel>(id, property.Name));
                break;
            }
        }

        return model;
    }

    public static Expression<Func<TModel, bool>> BuildKeyConditionExpression<TModel>(object id, string propertyName)
        where TModel : class
    {
        var parameter = Expression.Parameter(typeof(TModel), "model");
        var property = Expression.Property(parameter, propertyName);
        var constant = Expression.Constant(id);
        var equal = Expression.Equal(property, constant);
        var lambda = Expression.Lambda<Func<TModel, bool>>(equal, parameter);
        return lambda;
    }
}