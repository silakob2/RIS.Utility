using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using RestSharp;
using Utility.Package.Extensions;
using Utility.Package.UtilityModels;

namespace Utility.Package.UtilityServices;

public static class RestApiService
{
    #region Async

    #region HttpRequest

    public static Task<RestResponse> GetAsyncCore<TInput>(this HttpRequest httpRequest,
        TInput? input,
        string url)
    {
        var request = new RestRequest(url);
        return RestAsync(httpRequest, request, input);
    }

    public static Task<RestResponse> PostAsyncCore<TInput>(this HttpRequest httpRequest,
        TInput? input,
        string url)
    {
        var request = new RestRequest(url, Method.Post);
        return RestAsync<TInput>(httpRequest, request, input);
    }

    public static Task<RestResponse> PatchAsyncCore<TInput>(this HttpRequest httpRequest,
        TInput? input,
        string url)
    {
        var request = new RestRequest(url, Method.Patch);
        return RestAsync<TInput>(httpRequest, request, input);
    }

    public static Task<RestResponse> PutAsyncCore<TInput>(this HttpRequest httpRequest,
        TInput? input,
        string url)
    {
        var request = new RestRequest(url, Method.Put);
        return RestAsync<TInput>(httpRequest, request, input);
    }

    public static Task<RestResponse> DeleteAsyncCore<TInput>(this HttpRequest httpRequest,
        TInput? input,
        string url)
    {
        var request = new RestRequest(url, Method.Delete);
        return RestAsync<TInput>(httpRequest, request, input);
    }

    public static async Task<TOutPut?> ToResponseCore<TOutPut>(this Task<RestResponse> restResponse,
        string url)
    {
        var response = restResponse.Result;
        if (response.IsSuccessful)
        {
            TOutPut? output = default(TOutPut);
            if (response.Content != null)
            {
                output = response.Content.DeserializeCore<TOutPut?>();
            }

            return await Task.FromResult(output);
        }
        else
        {
            if (restResponse.Result.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                throw new ApiException(
                    ((int)restResponse.Result.StatusCode).ToString(),
                    $"API not found from : {url}",
                    url);
            }

            var msg = $"{restResponse.Result.ErrorMessage} - {Regex.Unescape(restResponse.Result.Content!)}";
            throw new ApiException(
                ((int)restResponse.Result.StatusCode).ToString(),
                msg,
                url);
        }
    }

    public static async Task<string?> ToResponseStringCore(this Task<RestResponse> restResponse,
        string url)
    {
        var response = restResponse.Result;
        if (response.IsSuccessful)
        {
            return await Task.FromResult(response.Content);
        }

        if (restResponse.Result.StatusCode == System.Net.HttpStatusCode.NotFound)
        {
            throw new ApiException(
                ((int)restResponse.Result.StatusCode).ToString(),
                $"API not found from : {url}",
                url);
        }

        var msg = $"{restResponse.Result.ErrorException?.Message} - {Regex.Unescape(restResponse.Result.Content!)}";
        throw new ApiException(
            ((int)restResponse.Result.StatusCode).ToString(),
            msg,
            url);
    }

    #endregion

    #region Private Methods

    private static Task<RestResponse> RestAsync<TInput>(HttpRequest httpRequest,
        RestRequest request, TInput? input)
    {
        try
        {
            var client = new RestClient();
            if (input != null)
            {
                Console.WriteLine($"Request : {input.SerializeCore()}");
                if (request.Method == Method.Get || request.Method == Method.Delete)
                {
                    if (IsSingleValueType(typeof(TInput?)))
                    {
                        request.AddQueryParameter("param", input.ToString());
                    }
                    else
                    {
                        var jCon = (JObject)JToken.FromObject(input);
                        AddQueryParam(request, jCon);
                    }
                }
                else
                {
                    var jsonToSend = input.SerializeCamelCaseCore();
                    request.AddJsonBody(jsonToSend);
                    request.RequestFormat = DataFormat.Json;
                }
            }

            var httpRequestHeaders = RequestHeaders(httpRequest);
            if (httpRequestHeaders.Count > 0)
            {
                client.AddDefaultHeaders(httpRequestHeaders);
            }

            var result = client.ExecuteAsync(request);
            return result;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    private static Dictionary<string, string> RequestHeaders(HttpRequest httpRequest)
    {
        var httpRequestHeaders = new Dictionary<string, string>();
        if (!string.IsNullOrEmpty(httpRequest.Headers["RequestID"]))
            httpRequestHeaders.Add("RequestID", httpRequest.Headers["RequestID"]!);
        if (!string.IsNullOrEmpty(httpRequest.Headers["Authorization"]))
            httpRequestHeaders.Add("Authorization", httpRequest.Headers["Authorization"]!);
        if (!string.IsNullOrEmpty(httpRequest.Headers["kpgHo"]))
            httpRequestHeaders.Add("kpgHo", httpRequest.Headers["kpgHo"]!);
        return httpRequestHeaders;
    }

    private static bool AddQueryParam(RestRequest request, JObject jObj, string? jKey = null)
    {
        var isAdded = false;
        try
        {
            foreach (var jTokenElement in jObj)
            {
                var key = jKey != null ? jKey + jTokenElement.Key.ToString() : jTokenElement.Key.ToString();
                JToken token = jTokenElement.Value;
                if (token is JArray)
                {
                    ProcessJArray(token, request, key);
                }
                else if (token is JObject propInObject)
                {
                    key += ".";
                    _ = AddQueryParam(request, propInObject, key);
                }
                else //property
                {
                    ProcessProperty(token, request, key, ref isAdded);
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

        return isAdded;
    }

    private static void ProcessProperty(JToken token, RestRequest request, string key, ref bool isAdded)
    {
        try
        {
            var value = token.Value<string>();
            if (!string.IsNullOrEmpty(value))
            {
                request.AddQueryParameter(key, token.Value<string>());
                isAdded = true;
            }
        }
        catch (Exception)
        {
            var value = token.Value<Guid>();
            if (Guid.Empty != value)
            {
                request.AddQueryParameter(key, value.ToString());
                isAdded = true;
            }
        }
    }

    private static void ProcessJArray(JToken token, RestRequest request, string key)
    {
        try
        {
            int count = 0;
            foreach (var obj in (JArray)token)
            {
                var keyInArray = $"{key}[{count}].";
                if (obj is JValue)
                {
                    var value = obj.Value<string>();
                    if (!string.IsNullOrEmpty(value))
                    {
                        request.AddQueryParameter(key, value);
                    }
                }
                else
                {
                    var objInArray = (JObject)obj;
                    var isSuccess = AddQueryParam(request, objInArray, keyInArray);
                    if (isSuccess) count++;
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    private static bool IsSingleValueType(Type type)
    {
        // Use Type.IsPrimitive property to check if the type is a single value type
        return type.IsPrimitive || type == typeof(decimal) || type == typeof(string) || type == typeof(int);
    }

    #endregion

    #endregion Async
}