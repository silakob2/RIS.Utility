﻿using System.Security.Claims;
using Utility.Package.UtilityModels;

namespace Utility.Package.UtilityServices.Interfaces;

public interface IClaimManagementService
{
    IEnumerable<Claim> GetClaims(ClaimsPrincipal claimsPrincipal);
    Actor? SetSessionIdToActor(IEnumerable<Claim> claims, string url);
    bool ValidateRoleInActor(Actor actor);
}