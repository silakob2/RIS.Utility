namespace Utility.Package.UtilityServices.Interfaces;

public interface IEncryptionService
{
    byte[] EncryptStringToBytes_Aes(string plainText);
    string DecryptStringFromBytes_Aes(byte[] cipherText);
    string Encrypt(string toEncryptString);
    string Decrypt(string encryptedString);
}