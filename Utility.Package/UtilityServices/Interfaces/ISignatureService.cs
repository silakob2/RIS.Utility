﻿namespace Utility.Package.UtilityServices.Interfaces;

public interface ISignatureService
{
    string SignSha256(string bodyString);
}