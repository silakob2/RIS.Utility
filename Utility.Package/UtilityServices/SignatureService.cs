using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Utility.Package.UtilityServices.Interfaces;

namespace Utility.Package.UtilityServices;

public class SignatureService : ISignatureService
{
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly string _privateKey = "private key here";

    public SignatureService(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    public string SignSha256(string bodyString)
    {
        HMACSHA256 hmac = new HMACSHA256(Encoding.UTF8.GetBytes(_privateKey));
        dynamic claim = JsonConvert.DeserializeObject(bodyString);
        claim.token = _httpContextAccessor.HttpContext.Request.Headers["Authorization"].ToString();
        var jsonToSign = JsonConvert.SerializeObject(claim);
        // Compute the HMAC for a message
        byte[] hashBytes = hmac.ComputeHash(Encoding.UTF8.GetBytes(jsonToSign));
        var signature = Convert.ToBase64String(hashBytes);

        hmac.Dispose();
        return signature;
    }
}