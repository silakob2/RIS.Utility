﻿using Microsoft.EntityFrameworkCore;
using Utility.Package.UtilityServices;

namespace Utility.Package.Extensions;

public abstract class ResponseExtension<T> where T : class
{
    protected ResponseExtension(){}

    protected ResponseExtension(DbContext context, string responseStatusCode, params string[] parameters)
    {
        (statusCode, statusDesc) = context.GetStatusByStatusCode<T>(responseStatusCode, parameters);
    }

    public int statusCode { get; set; }
    public string statusDesc { get; set; }
}