﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Utility.Package.Extensions;

public static class Extension
{
    private const string _formatDateTime = "yyyy-MM-dd HH:mm:ss";
    private const string _formatDate = "yyyy-MM-dd";
    private const string _formatDecimalTwoDigit = "0.00";

    public static string SerializeCore(this object obj)
    {
        try
        {
            return JsonSerializer.Serialize(obj, new JsonSerializerOptions
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
                PropertyNameCaseInsensitive = true,
                WriteIndented = true
            });
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    public static string SerializeCamelCaseCore(this object obj)
    {
        try
        {
            return JsonSerializer.Serialize(obj, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
                PropertyNameCaseInsensitive = true,
                WriteIndented = true
            });
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    public static T DeserializeCore<T>(this string json)
    {
        try
        {
            return JsonSerializer.Deserialize<T>(json, new JsonSerializerOptions
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
                PropertyNameCaseInsensitive = true,
                WriteIndented = true
            })!;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    public static T DeserializeCore<T>(this JsonElement json)
    {
        try
        {
            return JsonSerializer.Deserialize<T>(json, new JsonSerializerOptions
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
                PropertyNameCaseInsensitive = true,
                WriteIndented = true
            })!;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    #region String Methods

    public static string ToStringDateTimeCore(this DateTime datetime, string format = _formatDateTime)
    {
        return datetime.ToString(format);
    }

    public static string ToStringDateTimeCore(this DateTime? datetime, string format = _formatDateTime)
    {
        if (datetime == null) throw new ArgumentNullException("datetime");
        var newDateTime = datetime ?? DateTime.MinValue;
        return newDateTime.ToString(format);
    }

    public static string ToStringDateCore(this DateOnly date, string format = _formatDate)
    {
        return date.ToString(format);
    }

    public static string ToStringDateCore(this DateOnly? date, string format = _formatDate)
    {
        if (date == null) throw new ArgumentNullException("date");
        var newDateTime = date ?? DateOnly.MinValue;
        return newDateTime.ToString(format);
    }

    public static string ToStringGuidCore(this Guid value)
    {
        return value.ToString();
    }

    public static string ToStringGuidCore(this Guid? value)
    {
        if (value.IsNullOrEmptyGuidCore())
            return string.Empty;
        return value.ToString();
    }

    public static string ToStringIntCore(this int value)
    {
        return value.ToString();
    }

    public static string ToStringIntCore(this int? value)
    {
        if (value == null)
            return string.Empty;
        return value.ToString();
    }

    public static string ToStringDecimalCore(this decimal value, string format = _formatDecimalTwoDigit)
    {
        return value.ToString(format);
    }

    public static string ToStringDecimalCore(this decimal? value, string format = _formatDecimalTwoDigit)
    {
        if (value == null)
            return string.Empty;
        return ((decimal)value).ToString(format);
    }

    public static string ToStringFloatCore(this float value, string format = _formatDecimalTwoDigit)
    {
        return value.ToString(format);
    }

    public static string ToStringFloatCore(this float? value, string format = _formatDecimalTwoDigit)
    {
        if (value == null)
            return string.Empty;
        return ((float)value).ToString(format);
    }

    public static string ToStringDoubleCore(this double value, string format = _formatDecimalTwoDigit)
    {
        return value.ToString(format);
    }

    public static string ToStringDoubleCore(this double? value, string format = _formatDecimalTwoDigit)
    {
        if (value == null)
            return string.Empty;
        return ((double)value).ToString(format);
    }

    public static bool IsNullOrEmptyCore(this string? value)
    {
        return string.IsNullOrEmpty(value);
    }

    public static bool IsNullOrWhiteSpaceCore(this string value)
    {
        return string.IsNullOrWhiteSpace(value);
    }

    #endregion

    #region Guid Methods

    public static bool IsNullGuidCore(this Guid? value)
    {
        return value == null;
    }

    public static bool IsEmptyGuidCore(this Guid value)
    {
        return value == Guid.Empty;
    }

    public static bool IsNullOrEmptyGuidCore(this Guid? value)
    {
        return value == null || value == Guid.Empty;
    }

    #endregion

    #region Numeric

    public static int? ToIntCore(this string value)
    {
        if (int.TryParse(value, out var result))
            return result;
        return null;
    }

    public static int? ToIntCore(this object value)
    {
        try
        {
            return Convert.ToInt32(value);
        }
        catch
        {
            return null;
        }
    }

    public static decimal? ToDecimalCore(this string value)
    {
        if (decimal.TryParse(value, out var result))
            return result;
        return null;
    }

    public static decimal? ToDecimalCore(this object value)
    {
        try
        {
            return Convert.ToDecimal(value);
        }
        catch
        {
            return null;
        }
    }

    public static float? ToFloatCore(this string value)
    {
        if (float.TryParse(value, out var result))
            return result;
        return null;
    }

    public static float? ToFloatCore(this object value)
    {
        try
        {
            return Convert.ToSingle(value);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public static double? ToDoubleCore(this string value)
    {
        if (double.TryParse(value, out var result))
            return result;
        return null;
    }

    public static double? ToDoubleCore(this object value)
    {
        try
        {
            return Convert.ToSingle(value);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    #endregion
}