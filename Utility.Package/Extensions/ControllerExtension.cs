﻿using System.Runtime.CompilerServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Utility.Package.Extensions;

[Controller]
public abstract class ControllerExtension : ControllerBase
{
    /*Http-Header
    - X-Request-Id
    */
    public delegate object HandleDelegate(object request);

    public delegate object HandleDelegateWithNoRequest();

    protected readonly ILogger<ControllerExtension> _logger;

    protected ControllerExtension(ILogger<ControllerExtension> logger)
    {
        _logger = logger;
    }

    [NonAction]
    public void WriteLogRequestCore(string methodName, object request = null)
    {
        _logger.LogDebug("Method: {MethodName}", methodName);
        _logger.LogDebug(request == null ? " Request: null" : $" Request: {request.SerializeCore()}");
    }

    [NonAction]
    public void WriteLogResponseCore(object result)
    {
        _logger.LogDebug(result == null ? " Response: null" : $" Response: {result.SerializeCore()}");
    }

    [NonAction]
    public void WriteLogErrorCore(string methodName, Exception e)
    {
        _logger.LogError(e, "[{Name}][{MethodName}]: Error occured:{EMessage}", GetType().Name, methodName,
            e.Message);
    }

    [NonAction]
    public object ProcessControllerWithNoRequestCore(string methodName,
        HandleDelegateWithNoRequest handleMethodWithNoRequest)
    {
        try
        {
            WriteLogRequestCore(methodName);
            var response = handleMethodWithNoRequest();
            WriteLogResponseCore(response);
            return response;
        }
        catch (Exception e)
        {
            if (e.Message != null) _logger.LogError(e.Message);
            throw;
        }
    }

    [NonAction]
    protected object ProcessControllerCore(string methodName, object request, HandleDelegate handleMethod)
    {
        try
        {
            WriteLogRequestCore(methodName, request);
            var response = handleMethod(request);
            WriteLogResponseCore(response);
            return response;
        }
        catch (Exception e)
        {
            if (e.Message != null) _logger.LogError(e.Message);
            throw;
        }
    }
    
    protected delegate Task<object> HandleDelegateAsync(object request);
    public delegate Task<object> HandleDelegateWithNoRequestAsync();

    [NonAction]
    protected async Task<object> ProcessControllerCoreAsync(
        string methodName,
        object request,
        HandleDelegateAsync  handleMethod)
    {
        try
        {
            WriteLogRequestCore(methodName, request);
            object result = await handleMethod(request);
            WriteLogResponseCore(result);
            return result;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, ex.Message);
            throw;
        }
    }
    
    [NonAction]
    public async Task<object> ProcessControllerWithNoRequestCoreAsync(string methodName,
        HandleDelegateWithNoRequestAsync handleMethodWithNoRequest)
    {
        try
        {
            WriteLogRequestCore(methodName);
            var response = await handleMethodWithNoRequest();
            WriteLogResponseCore(response);
            return response;
        }
        catch (Exception e)
        {
            if (e.Message != null) _logger.LogError(e.Message);
            throw;
        }
    }

    [NonAction]
    public static string GetActualAsyncMethodName([CallerMemberName]string name = "") => name;
    
    #region ObjectResult

    [NonAction]
    public ObjectResult OkCore(object obj)
    {
        return new ObjectResult(obj)
        {
            StatusCode = StatusCodes.Status200OK
        };
    }

    [NonAction]
    public ObjectResult BadRequestCore(string message)
    {
        return new ObjectResult(message) { StatusCode = StatusCodes.Status400BadRequest };
    }

    [NonAction]
    public ObjectResult InternalServerErrorCore(string methodName, Exception e)
    {
        WriteLogErrorCore(methodName, e);
        return new ObjectResult(e.Message) { StatusCode = 599 };
    }

    #endregion
}