﻿using Microsoft.EntityFrameworkCore;

namespace Utility.Package.Extensions;

public abstract class RepositoryExtension<TContext> where TContext : DbContext
{
    protected readonly TContext _context;

    protected RepositoryExtension(TContext context)
    {
        _context = context;
    }
}