﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Utility.Package.Extensions;

public abstract class ServiceExtension<TContext> where TContext : DbContext
{
    protected readonly TContext _context;
    public readonly ILogger<ServiceExtension<TContext>> _logger;

    protected ServiceExtension(TContext context,
        ILogger<ServiceExtension<TContext>> logger)
    {
        _logger = logger;
        _context = context;
    }
}